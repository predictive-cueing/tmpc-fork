#pragma once

#include <boost/range/iterator_range.hpp>


namespace tmpc
{
    using boost::iterator_range;
    using boost::make_iterator_range;
}